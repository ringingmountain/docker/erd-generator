#!/usr/bin/env sh
set -e

. ci/common.sh

! echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}" 2>/dev/null

TAG_ARGS="--tag ${LATEST} --tag ${TAG}"
test -n "$1" && TAG_ARGS="${TAG_ARGS} --tag ${CI_REGISTRY_IMAGE}:${1}"

log_run 'Building Docker image' docker build --pull --no-cache $TAG_ARGS .
