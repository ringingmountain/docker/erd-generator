#!/usr/bin/env sh
set -e

# Colors
export COLOR_RESET="\033[0m"
export COLOR_GREY="\033[1;30m"
export COLOR_RED="\033[0;31m"
export COLOR_GREEN="\033[0;32m"
export COLOR_YELLOW="\033[0;33m"
export COLOR_BLUE="\033[1;34m"

export CI_REGISTRY=docker.io
export CI_REGISTRY_IMAGE=index.docker.io/ringingmountain/erd-generator

# CI_COMMIT_SHA is set by Gitlab CI, defaulting it allows running scripts outside of Gitlab CI
export CI_COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-parse HEAD | cut -b 1-7)}"

if test -n "${CI_COMMIT_TAG}"
then
  DOCKER_TAG="${CI_COMMIT_TAG}"
else
  DOCKER_TAG="$(echo "${CI_COMMIT_SHA}" | cut -c1-7)"
fi
LATEST="${CI_REGISTRY_IMAGE}:latest"
TAG="${CI_REGISTRY_IMAGE}:${DOCKER_TAG}"

log_run() {
  log_message "${1}"
  shift
  "$@"
  RET=$?

  if test ${RET} -eq 0
  then
    log_success
  else
    log_failure
  fi

  return ${RET}
}

log_message() {
  printf "%b%s%b\n" "${COLOR_BLUE}" "${*}" "${COLOR_RESET}"
}

log_success() {
  printf "%b%s%b\n" "${COLOR_GREEN}" "${1:-ok}" "${COLOR_RESET}"
}

log_warning() {
  printf "%b%s%b\n" "${COLOR_YELLOW}" "${MSG}" "${COLOR_RESET}"
}

log_failure() {
  printf "%b%s%b\n" "${COLOR_RED}" "${1:-failed}" "${COLOR_RESET}"
}

log_exit() {
  CODE=$?
  test ${CODE} -ne 0 && log_failure "exit ${CODE}"
  exit ${CODE}
}

trap log_exit EXIT
