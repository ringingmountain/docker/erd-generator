project = 'erd-generator'
copyright = '2019 Ringing Mountain, LLC'
author = 'Robin Klingsberg'

version = '0.1.0'

source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

htmlhelp_basename = "erd_generator_doc"
