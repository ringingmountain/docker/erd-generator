=============
ERD Generator
=============

Generate Entity Relationship Diagrams using the `erd`_ tool

.. csv-table::
   :widths: 10, 30

   "Repository",    "https://gitlab.com/ringingmountain/docker/erd-generator"
   "CI Pipeline",   "https://gitlab.com/ringingmountain/docker/erd-generator/pipelines"
   "Documentation", "https://erd-generator.readthedocs.io/"


|pipeline| |docs|


Usage
-----

.. code-block:: bash

   docker run \
     --user "$(id -u):$(id -g)" \
     -v "${PWD}/schemata":/schemata \
     -v "${PWD}/build":/build \
   ringingmountain/erd-generator:latest \
     -i /docs/file.er \
     -o /build/file.svg


Quickstart Development Guide
----------------------------

Build Documentation
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ci/docs.sh


Build Docker Image
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ./ci/build.sh [TAG]


Run Gitlab CI Jobs Locally
~~~~~~~~~~~~~~~~~~~~~~~~~~

You will first need to install the `Gitlab runner`_ package and `register`_ a runner on your local machine.

.. code-block:: bash

   gitlab-runner exec docker <job_name> --docker-services docker:dind --docker-privileged


Releasing a New Version
~~~~~~~~~~~~~~~~~~~~~~~

1. Checkout master.
2. Decide whether you are releasing a major, minor, or patch revision.
   For assistance in making this choice see the `SemVer`_ standard.
3. Ensure an entry for the version exists in ``CHANGELOG.rst`` summarizing the changes you are releasing.
4. Update the version in docs/conf.py
5. Commit the changes, commenting that you are bumping the version.
6. Tag the repo with the matching version.
7. Push to the central remote and your fork.



 .. |pipeline| image:: https://gitlab.com/ringingmountain/docker/erd-generator/badges/master/pipeline.svg
                :target: https://gitlab.com/ringingmountain/docker/erd-generator/pipelines

 .. |docs| image:: https://readthedocs.org/projects/erd-generator/badge/?version=latest
                :target: https://erd-generator.readthedocs.io/

.. _erd: https://github.com/BurntSushi/erd
.. _Gitlab runner: https://docs.gitlab.com/runner/install/
.. _register: https://docs.gitlab.com/runner/register/index.html#one-line-registration-command
.. _SemVer: https://semver.org
