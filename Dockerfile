FROM haskell

LABEL url="https://gitlab.com/ringingmountain/docker/erd-generator" authors='robin@ringingmountain.com'

RUN apt-get update && \
    apt-get -y install graphviz haskell-stack && \
    # Install erd
    git clone git://github.com/BurntSushi/erd && \
    cd erd && \
    stack install --install-ghc --local-bin-path /usr/local/bin && \
    cd / && \
    # Cleanup
    rm -rf erd /root/.stack && \
    apt-get -y remove haskell-stack && \
    apt-get -y autoremove && \
    apt-get clean

ENTRYPOINT ["/usr/local/bin/erd"]
